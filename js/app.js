var app = angular.module('myApp',['ngRoute','ngSanitize']);

app.config(['$routeProvider',function($routeProvider){
$routeProvider
    .when("/",{templateUrl:"partials/arrival.html",controller:"arrival"})
	.when("/book",{templateUrl:"partials/bookAdd.html",controller:"book"})
	.when("/add",{templateUrl:"partials/bookList.html",controller:"add"})
	.when("/empty",{templateUrl:"partials/bookEmpty.html",controller:"empty"})
	.when("/view",{templateUrl:"partials/bookView.html",controller:"view"})
	.when("/departure",{templateUrl:"partials/departure.html",controller:"departure"})
	.otherwise({redirectTo:"/"});
}]);